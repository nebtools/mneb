import { Partie } from '../model/partie';
import { Tour } from '../model/tour';

export class NebulaService {
	listParties(){
		let sparties:string=localStorage.getItem("parties");
		let parties:any=sparties?JSON.parse(sparties):{};
		return parties;
	}
	addPartie(name :string, tour:number){
		let parties=this.listParties();
		if(parties[name]===undefined)
			parties[name]={name:name, tours:tour};
		if(parties[name].tour===undefined || parties[name].tour<tour)
			parties[name].tour=tour;
		localStorage.setItem("parties",JSON.stringify(parties));
		return parties[name];
	}
	getCurrentPartie(){
		let current=localStorage.getItem("CurrentPartie");
		let parties=this.listParties();
		if(current) return parties[current];
		let names=Object.keys(parties);
		return parties[names[0]];
	}
	getPartieGlobal(name:string):Partie{
		let glob=localStorage.getItem("partie_"+name+"_global");
		return new Partie(JSON.parse(glob));
	}
	setPartieGlobal(partie:Partie){
		localStorage.setItem("partie_"+partie.Nom+"_global",JSON.stringify(partie.toJson()));
	}
	getPartieTour(name:string,tour:number):Tour{
		return new Tour(this.getTour(name,tour));
	}
	setPartieTour(tour:Tour){
		(this.setTour(tour.toJson()));
	}
	getCurrent(){
		return localStorage.getItem("CurrentPartie");
	}
	setCurrent(name:string){
		localStorage.setItem("CurrentPartie",name);
	}
	getTour(name :string,tour:number){
		let partie=localStorage.getItem("partie_"+name+"_"+tour);
		if(partie)
			return JSON.parse(partie);
		return {gen:{NomPartie:name}};
	}
	setTour(tour :any){
		localStorage.setItem("partie_"+tour.gen.NomPartie+"_"+tour.gen.Tour,JSON.stringify(tour));
	}
}