import { ICompo,Compo } from './compo';

export class Popup extends Compo {
	tagName="div";
	title:HTMLElement;
	clsbtn:Element;
	constructor(main:ICompo){
		super(main);
		this.elem.classList.add("popup");
		let header=this.addTag(this.elem,'div');
		header.classList.add('popupheader');
		this.title=this.addTag(header,'span');
		this.title.innerHTML="";
		this.clsbtn=this.addTag(header,'button');
		this.clsbtn.innerHTML="X";
		this.clsbtn.classList.add('btnclose');
		let _this=this;
		this.clsbtn.addEventListener('click',function(e){
			_this.close();
		});
	}
	close(){
		document.dispatchEvent(new CustomEvent('closePopup'));
	}
}