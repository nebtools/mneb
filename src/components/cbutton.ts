import { ICompo,Compo } from './compo';

export class CButton extends Compo{
	tagName="button";
	className='button';
	eventname:string;
	constructor(main:ICompo,label:string,eventname:string){
		super(main);
		this.elem.innerHTML=label;
		this.eventname=eventname;
		this.elem.addEventListener('click',this.click);
	}
	click(e:any){
		document.dispatchEvent(new CustomEvent(this.eventname));
	}
}