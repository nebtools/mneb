import {ICompo} from './compo';
import {Layer} from './layer';
import {Node} from './node';
import {Link} from './link';
import {NodeMenu} from './nodemenu';
import {SMonde} from '../model/partie';
import {INode,INodes} from '../model/inode';
import {ILink,ILinks} from '../model/ilink';

export class GMap {
	main:ICompo;
	div:HTMLElement;
	svg:SVGElement;
	defs:SVGElement;
	linkslayer:Layer;
	linklabslayer:Layer;
	nodeslayer:Layer;
	nodelabslayer:Layer;
	menulayer:Layer;
	menu:NodeMenu;
	nodes:INodes={};
	links:ILinks={};
	zoom:number;
	width:number;
	height:number;
	dragging:Dragging;
	eventPopupResize:any;
	constructor(main:ICompo) {
		this.main=main;
		this.div = document.createElement("div");
		this.main.elem.appendChild(this.div);
		this.div.innerHTML='test';
		this.svg=document.createElementNS('http://www.w3.org/2000/svg',"svg");
		this.main.elem.appendChild(this.svg);
		this.svg.setAttribute("ln","map");
		this.svg.setAttribute("width","200px");
		this.svg.setAttribute("height","200px");
		this.svg.onmousedown=function(e:Event){
			this.menu.close();
		}.bind(this);
		this.addDefs();
		this.addSymbols();
		this.linkslayer=new Layer(this,"links");
		this.linklabslayer=new Layer(this,"linklabs");
		this.nodeslayer=new Layer(this,"nodes");
		this.nodelabslayer=new Layer(this,"nodelabs");
		this.menulayer=new Layer(this,"menu");
		this.menu=new NodeMenu(this);
		this.zoom=40;
		this.eventPopupResize=function(e:CustomEvent){
			let rect = <DOMRect>e.detail.rect;
			let resized=false;
			if(this.width<rect.right){
				this.width=rect.right;
				resized=true;
			}
			if(this.height<rect.bottom){
				this.height=rect.bottom;
				resized=true;
			}
			if(resized){
				this.svg.setAttribute("width",this.width+"px");
				this.svg.setAttribute("height",this.height+"px");
			}
		}.bind(this);
		document.addEventListener("popupResize",this.eventPopupResize);
	}
	closePopup(){
		this.menu.close();
	}
	addTagTo(parent:SVGElement,name:string):SVGElement{
		let tag=<SVGElement>document.createElementNS('http://www.w3.org/2000/svg',name);
		parent.appendChild(tag);
		return tag;
	}
	addHTagTo(parent:HTMLElement,name:string):HTMLElement{
		let tag=document.createElement(name);
		parent.appendChild(tag);
		return tag;
	}
	addDefs(){
		this.defs=this.addTagTo(this.svg,"defs");
		NodeMenu.addDefs(this);
		Node.addDefs(this);
	}
	addDef(tagname:string,id:string){
		let ret= this.addTagTo(this.defs,tagname);
		ret.setAttribute('id',id);
		return ret;
	}
	addSymbols(){
		this.addSymbol('edit',[0,0,24,24],["M3 17.25v3.75h3.75l11.06-11.06-3.75-3.75-11.06 11.06zm17.71-10.21c.39-.39.39-1.02 0-1.41l-2.34-2.34c-.39-.39-1.02-.39-1.41 0l-1.83 1.83 3.75 3.75 1.83-1.83z"],["M0 0h24v24h-24z"]);
		this.addSymbol('delete',[0,0,24,24],["M6 19c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2v-12h-12v12zm13-15h-3.5l-1-1h-5l-1 1h-3.5v2h14v-2z"],["M0 0h24v24h-24z"]);
	}
	addSymbol(id:string,viewbox:number[],filled:string[],emptied:string[]){
		let s=<SVGSymbolElement>this.addTagTo(this.svg,'symbol')
		s.setAttribute('id',id);
		s.setAttribute('viewbox',viewbox.join(' '));
		for(let pd of filled){
			let p=this.addTagTo(s,'path');
			p.setAttribute('d',pd);
		}
		for(let pd of emptied){
			let p=this.addTagTo(s,'path');
			p.setAttribute('d',pd);
			p.setAttribute('fill','none');
		}
	}
	getLinksLayer(){
		return this.linkslayer;
	}
	getLinkLabsLayer(){
		return this.linklabslayer;
	}
	getNodesLayer(){
		return this.nodeslayer;
	}
	getNodeLabsLayer(){
		return this.nodelabslayer;
	}
	addNode(node:SMonde){
		this.nodes[node.id]=new Node(this,node);
	}
	resolveLinks(){
		for(let kn in this.nodes){
			for(let n of this.nodes[kn].cn){
				let sn='M_'+n
				let lid=(sn<kn?sn:kn)+"_"+(sn<kn?kn:sn);
				if(!this.links[lid] && this.nodes[sn]){
					this.links[lid]=new Link(this,this.nodes[sn<kn?sn:kn],this.nodes[sn<kn?kn:sn]);
					this.nodes[kn].links['C'+n]=this.links[lid];
					this.nodes[sn].links['C'+kn.substr(2)]=this.links[lid];
				}
			}
		}
	}
	clear(){
		for(let kl in this.links){
			this.links[kl].remove();
			delete this.links[kl];
		}	
		for(let kn in this.nodes){
			this.nodes[kn].remove();
			delete this.nodes[kn];
		}	
	}
	show(){
		this.div.innerHTML="show";
		for(let kl in this.links){
			this.links[kl].show();
		}	
		for(let kn in this.nodes){
			this.nodes[kn].show();
		}	
	}
	setSize(x:number,y:number){
		console.log(x,y);
		this.div.innerHTML="("+x+","+y+")";
		this.width=x;
		this.height=y;
		this.svg.setAttribute('width',''+(this.width*this.zoom)+'px');
		this.svg.setAttribute('height',''+(this.height*this.zoom)+'px');
	}
	dragStart(node:Node,e:MouseEvent){
		this.dragging=new Dragging(node,e);
	}
	changeZoom(zoom:number){
		this.zoom=zoom;
		for(let kn in this.nodes){
			this.nodes[kn].changeZoom();
			this.nodes[kn].show();
		}
		for(let kl in this.links){
			this.links[kl].show();
		}
		this.svg.setAttribute('width',''+(this.width*this.zoom)+'px');
		this.svg.setAttribute('height',''+(this.height*this.zoom)+'px');
	}
}

class Dragging{
	node:Node;
	ox:number;
	oy:number;
	ix:number;
	iy:number;
	moveEvent:any;
	upEvent:any;
	constructor(node:Node,e:MouseEvent){
		this.node=node;
		this.ox=e.clientX;
		this.oy=e.clientY;
		this.ix=node.x;
		this.iy=node.y;
		this.moveEvent=function(e:MouseEvent){
			this.dragMove(e);
		}.bind(this);
		this.upEvent=function(e:MouseEvent){
			this.dragStop(e);
		}.bind(this);
		document.addEventListener("mousemove",this.moveEvent);
		document.addEventListener("mouseup",this.upEvent);
	}
	dragMove(e:MouseEvent){
		this.node.x=this.ix-this.ox+e.clientX;
		this.node.y=this.iy-this.oy+e.clientY;
		this.node.show();
		this.node.showLinks();
	}
	dragStop(e:MouseEvent){
		document.removeEventListener("mousemove",this.moveEvent);
		document.removeEventListener("mouseup",this.upEvent);
		this.node.x=this.ix-this.ox+e.clientX;
		this.node.y=this.iy-this.oy+e.clientY;
		this.node.show();
		this.node.showLinks();
		this.node.drop();
	}
}