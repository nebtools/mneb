import { ICompo,Compo } from './compo';
import {MenuItem,IMenuItems} from './menuitem';

export class Menu extends Compo{
	main:ICompo;
	elem:HTMLElement;
	items:IMenuItems={};
	className="menudiv";
	constructor(main:ICompo){
		super(main);
		this.items['choose']=new MenuItem(this,'Partie','choose');
		this.items['zoomin']=new MenuItem(this,'Zoom+','zoom+');
		this.items['zoomout']=new MenuItem(this,'Zoom-','zoom-');
		this.items['nbt']=new MenuItem(this,'Import','addnbt');
		this.items['mndcr']=new MenuItem(this,'Paste CR','pasteCR');
		this.items['svnbt']=new MenuItem(this,'Save NBT','saveNBT');
	}
	show(){
		this.elem.innerHTML="";
		for(let ki in this.items)
			this.items[ki].show();
	}
}