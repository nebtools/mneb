import { ICompo,Compo } from './compo';

export class MenuItem{
	menu:ICompo;
	label:string;
	eventname:string;
	elem:HTMLElement;
	event:any;
	constructor(menu:ICompo,label:string,eventname:string){
		this.menu=menu;
		this.label=label;
		this.eventname=eventname;
	}
	show(){
		if(!this.elem){
			this.elem=document.createElement('div');
			this.elem.classList.add('menuitem');
			this.menu.elem.appendChild(this.elem);
		}
		this.elem.innerHTML=this.label;
		let _this=this;
		if(!this.event){
			this.event=function(e:Event){
				document.dispatchEvent(new CustomEvent(_this.eventname));
				e.preventDefault();
				return false;
			}
			this.elem.addEventListener('click',this.event);
		}
	}
	remove(){
		if(this.elem){
			if(this.event){
				this.elem.removeEventListener('click',this.event)
				delete this.event;
			}
			this.menu.elem.removeChild(this.elem);
			delete this.elem;
		}
	}
}

export interface IMenuItems{
	[index:string]:MenuItem;
}