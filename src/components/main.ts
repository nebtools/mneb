import {ICompo } from './compo';
import {Popup } from './popup';
import {GMap } from './gmap';
import {GPartie } from './gpartie';
import {Menu} from './menu';
import {ImportNbt} from './importnbt';
import {ImportMonde} from './importmonde';
import {NBT} from './nbt';
import {NebulaService} from '../services/nebula.service';
import {Tour} from '../model/tour';
import {ISMondes, SMonde,Partie} from '../model/partie';

export class Main implements ICompo {
	elem:HTMLElement;
	menu:Menu;
	map:GMap;
	partie:GPartie;
	eventClosePopup:any;
	eventImport:any;
	eventChoose:any;
	eventAddTour:any;
	eventOpenMap:any;
	eventUpdateNode:any;
	eventZoomPlus:any;
	eventZoomMoins:any;
	eventPasteCR:any;
	eventAddMondeCR:any;
	eventDeleteNode:any;
	eventUpdateComment:any;
	eventSaveNbt:any;
	popup:Popup;
	neb:NebulaService;
	divtours:HTMLElement;
	constructor(master:HTMLElement){
		this.elem=master;
		this.menu=new Menu(this);
		this.map=new GMap(this);
		this.divtours=document.createElement('div');
		master.appendChild(this.divtours);
		this.partie=new GPartie(this);
		this.neb=new NebulaService();
	}
	addEvents(){
		this.eventClosePopup=(function(e:any){
			this.closePopup();
		}).bind(this);
		document.addEventListener("closePopup",this.eventClosePopup);
		this.eventImport=(function(e:any){
			e.stopPropagation();
			this.openImport();
		}).bind(this);
		document.addEventListener("addnbt",this.eventImport);
		this.eventChoose=(function(e:any){
			e.stopPropagation();
			this.openChoose();
		}).bind(this);
		document.addEventListener("choose",this.eventChoose);
		this.eventAddTour=(function(e:CustomEvent){
			e.stopPropagation();
			this.addTour(e.detail);
		}).bind(this);
		document.addEventListener("addTour",this.eventAddTour);
		this.eventOpenMap=(function(e:CustomEvent){
			e.stopPropagation();
			this.openMap();
		}).bind(this);
		document.addEventListener("openMap",this.eventOpenMap);
		this.eventUpdateNode=(function(e:CustomEvent){
			e.stopPropagation();
			this.updateNode(e.detail);
		}).bind(this);
		document.addEventListener("updateNode",this.eventUpdateNode);
		this.eventZoomPlus=(function(e:CustomEvent){
			e.stopPropagation();
			this.zoomPlus(e.detail);
		}).bind(this);
		document.addEventListener("zoom+",this.eventZoomPlus);
		this.eventZoomMoins=(function(e:CustomEvent){
			e.stopPropagation();
			this.zoomMoins(e.detail);
		}).bind(this);
		document.addEventListener("zoom-",this.eventZoomMoins);
		this.eventPasteCR=(function(e:CustomEvent){
			e.stopPropagation();
			this.openPasteCR();
		}).bind(this);
		document.addEventListener("pasteCR",this.eventPasteCR);
		this.eventAddMondeCR=(function(e:CustomEvent){
			e.stopPropagation();
			this.addMondeCR(e.detail);
		}).bind(this);
		document.addEventListener("addMondeCR",this.eventAddMondeCR);
		this.eventDeleteNode=(function(e:CustomEvent){
			e.stopPropagation();
			this.deleteNode(e.detail.id);
		}).bind(this);
		document.addEventListener("deleteNode",this.eventDeleteNode);
		this.eventUpdateComment=(function(e:CustomEvent){
			e.stopPropagation();
			this.updateComment(e.detail.id,e.detail.comment);
		}).bind(this);
		document.addEventListener("updateComment",this.eventUpdateComment);
		this.eventSaveNbt=(function(e:CustomEvent){
			e.stopPropagation();
			this.saveNbt();
		}).bind(this);
		document.addEventListener("saveNBT",this.eventSaveNbt);

	}
	show(){
		this.menu.show();
		this.openMap();
		this.listTours();
		this.addEvents();
	}
	remove(){
		if(this.eventImport){
			document.removeEventListener("addnbt",this.eventImport);
			delete this.eventImport;
		}
	}
	closePopup(){
		if(this.popup){
			this.popup.remove();
			delete this.popup;
		}
		this.map.closePopup();
	}
	openImport(){
		this.closePopup();
		this.popup=new ImportNbt(this);
		console.log("openImport");
	}
	openChoose(){
		this.closePopup();
		console.log(this);
	}
	addTour(tour:any){
		console.log(tour);
		this.neb.addPartie(tour.gen.NomPartie,tour.gen.Tour);
		this.neb.setTour(tour);
		let otour=new Tour(tour);
		console.log(otour);
		let partie:Partie=this.neb.getPartieGlobal(tour.gen.NomPartie);
		if(partie.Tour<otour.g.Tour)
			partie.Tour=otour.g.Tour;
		if(partie.Nom!=otour.g.NomPartie)
			partie.Nom=otour.g.NomPartie;
		partie.SizeX=otour.g.SizeX;
		partie.SizeY=otour.g.SizeY;
		for(let km in otour.m){
			let om=otour.m[km];
			if(!partie.m[km])
				partie.m[km]=new SMonde(km,om);
			if(!partie.m[km].x)
				partie.m[km].x=om.x;
			if(!partie.m[km].y)
				partie.m[km].y=om.y;
			if(om.cn.length>partie.m[km].cn.length){
				partie.m[km].cn=[];
				for(let c of om.cn)
					partie.m[km].cn.push(c);
			}
		}
		this.neb.setPartieGlobal(partie);
		this.neb.setCurrent(partie.Nom);
		this.closePopup();
		document.dispatchEvent(new CustomEvent("openMap"));
	}
	openMap(){
		let nom =this.neb.getCurrent();
		if(nom){
			this.map.clear();
			let partie:Partie = this.neb.getPartieGlobal(nom);	
			this.map.setSize(partie.SizeX,partie.SizeY);
			for(let kn in partie.m)
				this.map.addNode(partie.m[kn]);
			this.map.resolveLinks();
			let tour:Tour=this.neb.getPartieTour(partie.Nom,partie.Tour);
			for(let kn in tour.m)
				this.map.nodes[kn].setTour(tour.m[kn]);
			for(let kf in tour.f){
				let f=tour.f[kf];
				if(f.Localisation)
					this.map.nodes['M_'+f.Localisation].addFlotte(f);
				if(f.ch&&f.ch.length>0)
					for(let c of f.ch)
						if(this.map.nodes['M_'+c])
							this.map.nodes['M_'+c].addTrace(f);
			}
			this.map.show();
		}
	}
	listTours(){
		this.divtours.innerHTML="";
		let fld=document.createElement("input");
		this.divtours.appendChild(fld);
		fld.setAttribute('type','text');
	}
	updateNode(upd:any){
		let nom =this.neb.getCurrent();
		if(nom){
			let partie:Partie = this.neb.getPartieGlobal(nom);	
			partie.m[upd.id].x=upd.x;
			partie.m[upd.id].y=upd.y;
			this.neb.setPartieGlobal(partie);
		}
	}
	zoomPlus(){
		console.log('+');
		if(this.map.zoom==25)
			this.map.changeZoom(40);
		else if(this.map.zoom==40)
			this.map.changeZoom(60);
	}
	zoomMoins(){
		console.log('-');
		if(this.map.zoom==60)
			this.map.changeZoom(40);
		else if(this.map.zoom==40)
			this.map.changeZoom(25);
	}
	openPasteCR(){
		this.closePopup();
		this.popup=new ImportMonde(this);
		console.log("openPasteCR");
	}
	deleteNode(id:string){
		let nom=this.neb.getCurrent();
		let glob=this.neb.getPartieGlobal(nom);
		let partie=new Partie(glob);
		for(let icn in partie.m[id].cn)	{
			let mid="M_"+icn;
			let oid=parseInt(id.substr(2));
			if(partie.m[mid]){
				if(partie.m[mid].cn.indexOf(oid)>-1){
					partie.m[mid].cn.slice(partie.m[mid].cn.indexOf(oid),1);
				}
			}
		}
		delete partie.m[id];
		this.neb.setPartieGlobal(partie);
		document.dispatchEvent(new CustomEvent("openMap"));
	}
	addMondeCR(mcr:string){
		let nom=this.neb.getCurrent();
		let glob=this.neb.getPartieGlobal(nom);
		let partie=new Partie(glob);
		let tour = this.neb.getPartieTour(nom,partie.Tour);
		let pm=NBT.parseMonde(tour,mcr);
		let m=pm.m;
		console.log(m.toNBT());
		let pt=tour.g.Tour==pm.t?tour:this.neb.getPartieTour(nom,pm.t);
		let sm=partie.m[m.id];
		pt.m[m.id]=m;
		console.log(m);
		if(!sm){
			sm=new SMonde(m.id,null);
			sm.x=0;
			sm.y=0;
			partie.m[m.id]=sm;
		}
		sm.cn=m.cn;
		if(!sm.t)
			sm.t={};
		console.log(sm);
		for(let ic of sm.cn){
			let idc='M_'+ic;
			if(!partie.m[idc]){
				let smc=new SMonde(idc,null);
				smc.x=0;
				smc.y=0;
				smc.cn=[parseInt(m.id.substr(2))];
				partie.m[idc]=smc;
			}
		}
		console.log(pt);
		this.neb.setTour(pt.toJson());
		this.neb.setPartieGlobal(partie);
		this.closePopup();
		document.dispatchEvent(new CustomEvent("openMap"));
	}
	updateComment(id:string,comment:string){
		let nom=this.neb.getCurrent();
		let glob=this.neb.getPartieGlobal(nom);
		let partie=new Partie(glob);
		partie.m[id].comment=comment;
		console.log(partie.toJson());
		this.neb.setPartieGlobal(partie);
	}
	saveNbt(){
		let nom=this.neb.getCurrent();
		let glob=this.neb.getPartieGlobal(nom);
		let partie=new Partie(glob);
		let tour = this.neb.getPartieTour(nom,partie.Tour);
		console.log(tour.toNBT());
	}
}