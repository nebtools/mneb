import {GCompo} from './gcompo';
import {SMonde} from '../model/partie';
import {Monde} from '../model/monde';
import {IFlottes,Flotte} from '../model/flotte';
import {INode} from '../model/inode';
import {ILinks} from '../model/ilink';
import {GMap} from './gmap';

export class Node implements INode {
	id:string;
	map:GMap
	g:SVGElement ;
	b:SVGElement ;
	t:SVGElement ;
	links:ILinks={};
	cn:number[];
	x:number;
	y:number;
	nx:number;
	ny:number;
	label:string;
	dragStart:any;
/*	tl:NodeOrbe;
	tr:NodeOrbe;
*/	minorbes:GCompo[]=[];
	midorbes:GCompo[]=[];
	maxorbes:GCompo[]=[];
	tour:Monde;
	flottes:IFlottes={};
	traces:IFlottes={};
	comment:string;
	o_ind:NodeSat;
	o_pop:NodeSat;
	static addDefs(map:GMap){
		map.addDef('path','indorbemin').setAttribute('d','M -12 0  A 12 12 0 0 1 0 -12');
		map.addDef('path','indorbemid').setAttribute('d','M -12 0 L -19 0 A 19 19 0 0 1 0 -19 L 0 -12 A 12 12 0 0 0 -12 0');
		map.addDef('path','indorbemidtext').setAttribute('d','M -13 0  A 13 13 0 0 1 0 -13');
		map.addDef('path','indorbevimid').setAttribute('d','M -22 -22 A 40 40 0 0 1 -10 -22 L -10 -16 A 8 16 0 0 1 -16 -10 A 8 16 0 0 1 -22 -16 L -22 -22');
		map.addDef('path','indorbemax').setAttribute('d','M -12 0 L -28 0 A 28 28 0 0 1 0 -28 L 0 -12 A 12 12 0 0 0 -12 0');
		map.addDef('path','indorbemaxtext').setAttribute('d','M -16 0  A 16 16 0 0 1 0 -16');
		map.addDef('path','indorbevimax').setAttribute('d','M -32 -32 A 40 40 0 0 1 -20 -32 L -20 -26 A 8 16 0 0 1 -26 -20 A 8 16 0 0 1 -32 -26 L -32 -32');
		map.addDef('path','poporbemin').setAttribute('d','M 12 0  A 12 12 0 0 1 0 -12');
		map.addDef('path','poporbemid').setAttribute('d','M 12 0 L 22 0 A 19 19 0 0 0 0 -19 L 0 -12 A 12 12 0 0 1 12 0');
		map.addDef('path','poporbemidtext').setAttribute('d','M 0 -13 A 13 13 0 0 1 13 0');
		map.addDef('path','poporbevpmid').setAttribute('d','M 22 -22 A 40 40 0 0 1 10 -22 L 10 -16 A 8 16 0 0 0 16 -10 A 8 16 0 0 0 22 -16 L 22 -22');
		map.addDef('path','poporbemax').setAttribute('d','M 12 0 L 28 0 A 28 28 0 0 0 0 -28 L 0 -12 A 12 12 0 0 1 12 0');
		map.addDef('path','poporbemaxtext').setAttribute('d','M 0 -16 A 16 16 0 0 1 16 0');
		map.addDef('path','poporbevpmax').setAttribute('d','M 32 -32 A 40 40 0 0 0 20 -32 L 20 -26 A 8 16 0 0 0 26 -20 A 8 16 0 0 0 32 -26 L 32 -32');
	}
	constructor(map:GMap,node:SMonde){
		if(node.id=='M_116')
			console.log(node);
		this.map=map;
		this.g=map.getNodesLayer().addTag('g');
		this.b=map.addTagTo(this.g,'circle');
		this.b.setAttribute('cx','0px');
		this.b.setAttribute('cy','0px');
		this.b.classList.add('nodecontainer');
		this.dragStart=function(e:MouseEvent){
			e.preventDefault();
			this.map.dragStart(this,e);
		}.bind(this);
		this.b.addEventListener("mousedown",this.dragStart);
		this.t=map.addTagTo(this.g,'text');
		this.t.setAttribute('x','0px');
		this.t.setAttribute('y','3px');
		this.t.innerHTML=node.id.substr(2);
		this.t.classList.add('nodelabel');
		this.t.addEventListener("mousedown",this.dragStart);
		this.nx=node.x?node.x:0;
		this.ny=node.y?node.y:0;
		this.id=node.id;
		this.cn=node.cn;
		this.comment=node.comment;
		this.o_ind=new NodeSat(this);
		this.o_pop=new NodeSat(this);
/*		this.tl=new NodeOrbe(this,Orbe.IndsMin,Orbe.IndsMid,Orbe.IndsMax);
		this.tr=new NodeOrbe(this,Orbe.PopsMin,Orbe.PopsMid,Orbe.PopsMax);
*/		this.changeZoom();
	}
	changeZoom(){
		this.x=this.nx*this.map.zoom;
		this.y=this.ny*this.map.zoom;
		switch(this.map.zoom){
		case 60:
			this.o_ind.setPaths('#indorbemax','#indorbemaxtext');
			this.o_ind.setTextClass('orbetextmax');
			this.o_pop.setPaths('#poporbemax','#poporbemaxtext')
			this.o_pop.setTextClass('orbetextmax');
			break;
		case 40:
			this.o_ind.setPaths('#indorbemid','#indorbemidtext')
			this.o_ind.setTextClass('orbetextmid');
			this.o_pop.setPaths('#poporbemid','#poporbemidtext')
			this.o_pop.setTextClass('orbetextmid');
			break;
		default:
			this.o_ind.setPaths('#indorbemin',null)
			this.o_pop.setPaths('#poporbemin',null)
		}
/*		for(let orb of this.minorbes) this.map.zoom==25?orb.show():orb.hide();
		for(let orb of this.midorbes) this.map.zoom==40?orb.show():orb.hide();
		for(let orb of this.maxorbes) this.map.zoom==60?orb.show():orb.hide();
*//*		this.tl.changeZoom();
		this.tr.changeZoom();
*/	}
	remove(){
		this.map.getNodesLayer().removeTag(this.g);
	}
	setTour(m:Monde){
		this.tour = m;
		let i=this.minorbes.length;
		while(i--){
/*			this.minorbes[i].removeMe();
			this.midorbes[i].removeMe();
			this.maxorbes[i].removeMe();
			delete this.minorbes[i];
			delete this.midorbes[i];
			delete this.maxorbes[i];
*/		}
		if(m.Ind>0){
			let text=m.IndLibre+"/"+m.Ind;
/*			this.minorbes.push(new GCompo(this.map.getNodesLayer(),this.g,Orbe.IndsMin,'orbemin',-6,-4,null));
			this.midorbes.push(new GCompo(this.map.getNodesLayer(),this.g,Orbe.IndsMid,'orbemid',-12,-8,text));
			this.maxorbes.push(new GCompo(this.map.getNodesLayer(),this.g,Orbe.IndsMax,'orbemax',-20,-16,text));
*/			this.o_ind.setText(text);
			this.o_ind.show();
		}else{
			this.o_ind.hide();
		}
		if(m.MaxPop>0||(m.Pop+m.Conv+m.Robot)>0){
			let text=m.Robot>0?(m.Robot+"R"):(m.Pop+(m.Pop==m.Conv?"C":""));
			text+="("+m.MaxPop+")";
			if(m.Pop>m.Conv && m.Conv>0)
				text+="C";
			this.o_pop.setText(text);
			this.o_pop.show();
/*			this.minorbes.push(new GCompo(this.map.getNodesLayer(),this.g,Orbe.PopsMin,'orbemin',6,-4,null));
			this.midorbes.push(new GCompo(this.map.getNodesLayer(),this.g,Orbe.PopsMid,'orbemid',2,-8,text));
			this.maxorbes.push(new GCompo(this.map.getNodesLayer(),this.g,Orbe.PopsMax,'orbemax',2,-16,text));
*/		}else{
			this.o_pop.hide();
		}
		this.changeZoom();
	}
	addFlotte(f:Flotte){
		this.flottes[f.id]=f;
	}
	addTrace(f:Flotte){
		this.traces[f.id]=f;
	}
	show(){
		this.g.setAttribute('transform','translate('+this.x+','+this.y+')');
/*		this.b.setAttribute("cx",""+this.x+"px");
		this.b.setAttribute("cy",""+this.y+"px");
		this.t.setAttribute("x",""+this.x+"px");
		this.t.setAttribute("y",""+(this.y+3)+"px");
*/	}
	showLinks(){
		for(let kl in this.links){
			this.links[kl].show();
		}
	}
	drop(){
		let nx=Math.round(this.x/this.map.zoom);
		let ny=Math.round(this.y/this.map.zoom);
		console.log(nx,ny,this.nx,this.ny);
		if(this.nx==nx&&this.ny==ny){
			this.changeZoom();
			this.show();
			this.showLinks();
			document.dispatchEvent(new CustomEvent("openMenu",{detail:this}));
		}
		else{
			this.nx=nx;
			this.ny=ny;
			this.changeZoom();
			this.show();
			this.showLinks();
			document.dispatchEvent(new CustomEvent("updateNode",{detail:{id:this.id,x:this.nx,y:this.ny}}));
		}
	}
	getFlottesCR():string{
		let res="";
		for(let kf in this.flottes){
			let f= this.flottes[kf];
			res+="\t "+f.getCR()+'\n';
		}
		let nid=parseInt(this.id.substr(2));
		for(let kf in this.traces){
			let f= this.traces[kf];
			res+="\t"+f.getTrace(nid)+'\n';
		}
		return res;
	}
}

enum Orbe {
	IndsMin="M -11 0 A 12 12 0 0 1 0 -11",
	IndsMid="M -11 0 L -16 0 L -16 -13 A 3 3 0 0 1 -13 -16 L 0 -16 L 0 -11 A 12 12 0 0 0 -11 0",
	IndsMax="M -11 0 L -26 0 L -26 -23 A 3 3 0 0 1 -23 -26 L 0 -26 L 0 -11 A 12 12 0 0 0 -11 0",
	PopsMin="M 11 0 A 11 11 0 0 0 -11 0",
	PopsMid="M 11 0 L 16 0 L 16 -13 A 3 3 0 0 0 13 -16 L 0 -16 L 0 -11 A 11 11 0 0 1 11 0",
	PopsMax="M 11 0 L 26 0 L 26 -23 A 3 3 0 0 0 23 -26 L 0 -26 L 0 -11 A 11 11 0 0 1 11 0",
	OIndsMin="M -12 -1 A 12 12 0 0 1 -1 -12",
	OIndsMid="M -12 -1 L -17 -1 L -17 -14 A 3 3 0 0 1 -14 -17 L -1 -17 L -1 -12 A 12 12 0 0 0 -12 -1",
	OIndsMax="M -12 -1 L -27 -1 L -27 -24 A 3 3 0 0 1 -24 -27 L -1 -27 L -1 -12 A 12 12 0 0 0 -12 -1",
	OPopsMin="M 12 -1 A 12 12 0 0 0 -12 1",
	OPopsMid="M 12 -1 L 17 -1 L 17 -14 A 3 3 0 0 0 14 -17 L 1 -17 L 1 -12 A 12 12 0 0 1 12 -1",
	OPopsMax="M 12 -1 L 27 -1 L 27 -24 A 3 3 0 0 0 24 -27 L 1 -27 L 1 -12 A 12 12 0 0 1 12 -1"
}
/*
class NodeOrbe {
	node:Node;
	p:SVGElement;
	min:Orbe;
	mid:Orbe;
	max:Orbe;
	constructor(node:Node,min:Orbe,mid:Orbe,max:Orbe,dx:number,dy:number){
		this.node=node;
		this.min=min;
		this.mid=mid;
		this.max=max;
		this.p=this.node.map.getNodesLayer().addTagTo(this.node.g,'path');
		this.p.classList.add('orbe');
		this.changeZoom();
	}
	changeZoom(){
		switch(this.node.map.zoom){
			case 60:
				this.p.setAttribute('d',this.max);
				this.p.classList.remove("orbemin","orbemid");
				this.p.classList.add("orbemax");
				for(let orb in this.minorbes) orb.hide();
				for(let orb in this.midorbes) orb.hide();
				for(let orb in this.maxorbes) orb.show();
				break;
			case 40:
				this.p.setAttribute('d',this.mid);
				this.p.classList.remove("orbemin","orbemax");
				this.p.classList.add("orbemid");
				for(let orb in this.minorbes) orb.hide();
				for(let orb in this.midorbes) orb.show();
				for(let orb in this.maxorbes) orb.hide();
				break;
			default:
				this.p.setAttribute('d',this.min);
				this.p.classList.remove("orbemax","orbemid");
				this.p.classList.add("orbemin");
				for(let orb in this.minorbes) orb.show();
				for(let orb in this.midorbes) orb.hide();
				for(let orb in this.maxorbes) orb.hide();
		}
	}
}*/
class NodeSat{
	node:Node;
	t:SVGTextElement;
	p:SVGUseElement;
	tp:SVGTextPathElement;
	constructor(node:Node){
		this.node=node;
		this.p=<SVGUseElement>node.map.addTagTo(node.g,'use');
		this.p.classList.add('menupath');
		this.t=<SVGTextElement>node.map.addTagTo(node.g,'text');
		this.tp=<SVGTextPathElement>node.map.addTagTo(this.t,'textPath');
		this.tp.setAttribute('startOffset','50%');
	}
	setPaths(idP:string,idT:string){
		this.p.setAttribute('href',idP);
		if(idT)
			this.tp.setAttribute('href',idT);
		else{
			this.tp.removeAttribute('href');
			this.tp.style.visibility='hidden';
		}
	}
	setText(txt:string){
		this.tp.innerHTML=txt;
	}
	setTextClass(classname:string){
		let i=this.t.classList.length;
		while(i--)
			this.t.classList.remove(this.t.classList[i]);
		this.t.classList.add(classname);
	}
	show(){
		if(this.tp.hasAttribute('href'))
			this.t.style.visibility='show';
		this.p.style.visibility='show';
	}
	hide(){
		this.t.style.visibility='hidden';
		this.p.style.visibility='hidden';
	}
}