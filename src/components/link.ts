import {GMap} from './gmap';
import { INode } from '../model/inode';

export class Link {
	map :GMap;
	from :INode;
	to:INode;
	p:SVGElement;
	constructor(map:GMap,from:INode,to:INode){
		this.map=map;
		this.from=from;
		this.to=to;
		this.p=map.getLinksLayer().addTag('path');
		this.p.classList.add('link');
	}
	remove(){
		this.map.getLinksLayer().removeTag(this.p);
	}
	show(){
		let path="M "+this.from.x+","+this.from.y+" L "+this.to.x+","+this.to.y;
		this.p.setAttribute('d',path);
	}
}