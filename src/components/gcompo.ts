import { Layer } from './layer';

export class GCompo{
	gg:Layer;
	parent:SVGElement;
	g:SVGElement;
	p:SVGElement;
	t:SVGElement;
	text:string;
	constructor(gg:Layer,parent:SVGElement,d:string,cl:string,x:number,y:number,text:string){
		this.gg=gg;
		this.parent=parent;
		this.g=gg.map.addTagTo(parent,'g');
		this.g.classList.add(cl);
		this.p=gg.map.addTagTo(this.g,'path');
		this.p.setAttribute('d',d);
		if(text){
			this.t=gg.map.addTagTo(this.g,'text');
			this.t.setAttribute('x',x+'px');
			this.t.setAttribute('y',y+'px');
			this.text=text;
			this.t.classList.add(cl);
			this.t.innerHTML=text;
		}
		this.hide();
	}
	show(){
		this.p.style.visibility='visible';
		if(this.t)
			this.t.style.visibility='visible';
	}
	hide(){
		this.p.style.visibility='hidden';
		if(this.t)
			this.t.style.visibility='hidden';
	}
	moveTo(x:number,y:number){
		this.g.setAttribute('transform','translate('+x+','+y+')');
	}
	removeMe(){
		this.g.removeChild(this.p);
		if(this.t)
			this.g.removeChild(this.t);
		this.parent.removeChild(this.p);
	}
}

