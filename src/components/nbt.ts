import {Tour} from '../model/tour';
import {Monde} from '../model/monde';
import {Flotte} from '../model/flotte';
export class NBT {

	static parseMonde(tour:Tour,s:string):{t:number,m:Monde,f:Flotte[] }{
		let st=0;
		let i=0;
		let toks=s.split(/\s+/);
		console.log(toks);
		let state=-1;
		let monde=new Monde("new",tour,{});
		monde.hasC=true;
		monde.hasM=true;
		monde.hasEx=true;
		monde.hasME=true;
		let flottes:Flotte[]=[];
		let numt=tour.g.Tour;
		while(i<toks.length){
			if(toks[i].length==0){
				i++;
				continue;
			}
			console.log(state,toks[i]);
			switch(state){
			case -1:
				if(toks[i]=='###'){
					i++;
					let com="###"
					while(toks[i].charAt(0)!='"' && !toks[i].match(/^M_\d+$/)){
						com+=" "+toks[i];
						i++;
					}
					console.log(com);
					let ift=com.match(/^### info.*tour (\d+)$/i);
					if(ift){
						console.log(ift);
						numt=parseInt(ift[1]);
					}
				}
				console.log(numt);
				state=0;
				break;
			case 0:
				if(toks[i].charAt(0)=='"')
					state=1;
				else if(toks[i].charAt(0)=='M')
					state=2;
				else i++;
				break;
			case 1: // nom
				monde.Nom=toks[i];
				while(toks[i].charAt(toks[i].length-1)!='"')
					monde.Nom+=' '+toks[++i];
				state++;
				i++;
				break;
			case 2: //monde
				monde.id=toks[i++];
				state++;
				break;
			case 3: //trou noir ?
				if(toks[i]=="***" && toks[i+1]=='Trou' && toks[i+2]=='noir' && toks[i+3]=='***'){
					monde.TN=true;
					i+=4;
					state=20;
				} else if(toks[i].charAt(0)=='('){
					let cn=toks[i].substr(1,toks[i].length-2).split(',');
					for(let c of cn)
						monde.cn.push(parseInt(c));
					state=4;
					i++;
				} else state=100;
				break;
			case 4: // nom
				let pNom=toks[i];
				while(!toks[i].match(/.*"!?C?$/))
					pNom+=' '+toks[++i];
				console.log(pNom); // TODO find jid
				if(pNom.charAt(pNom.length-1)=='C'){
					monde.Cadeau=true;
					pNom=pNom.substr(0,pNom.length-1);
				}
				if(pNom.charAt(pNom.length-1)=='P'){
					monde.Pille=true;
					pNom=pNom.substr(0,pNom.length-1);
				}
				if(pNom.charAt(pNom.length-1)=='!'){
					monde.Capture=true;
					pNom=pNom.substr(0,pNom.length-1);
				}
				pNom=pNom.substring(1,pNom.length-1);
				if(pNom.charAt(pNom.length-1)==')'){
					let ppNom=pNom.substring(pNom.indexOf('(')+1,pNom.length-1);
					pNom=pNom.substring(0,pNom.indexOf('('));
					let idpJ=tour.getIdJoueur(ppNom);
					console.log("previous owner "+ppNom+"("+idpJ+")");
				}else if(pNom.indexOf(':')>-1){
					let nbTours=pNom.substring(pNom.indexOf(':')+1,pNom.length);
					monde.Duree=parseInt(nbTours);
					pNom=pNom.substring(0,pNom.indexOf(':'));
				}
				let idJ=tour.getIdJoueur(pNom);
				console.log("owner "+pNom+"("+idJ+")");
				monde.Proprio=parseInt(idJ.substr(2));
				state++;
				i++;
				break;
			case 5: //ind
				let indm=toks[i].match(/^\[?(I=(\d+)(?:\/(\d+))?)?(?:\]=(\d+))?(\*(\*|F_\d+))?$/);
				if(indm){
					console.log(indm);
					monde.IndLibre=parseInt(indm[2]);
					monde.Ind=parseInt(indm[2]?indm[3]:indm[2]);
					monde.VI=indm[4]?parseInt(indm[4]):0;
					monde.VP=indm[5]?parseInt(indm[5]):0;
					i++;
				} 
				state=6;
				break;
			case 6: //pops
				let popm=toks[i].match(/^\[?P=(\d+)(R)?(C)?(?:\{-(\d+)P\})?(?:\{-(\d+)C\})?(?:\{-(\d+)R\})?\((\d+)\)(?:\/(\d+)C"([^"]+)")?(?:\]=(\d+))?/);
				if(popm){
					monde.MaxPop=parseInt(popm[7]);
					monde.Pop=parseInt(popm[1]);
					//TODO
					i++;
				}
				state=7;
				break;
			case 7:
				let mpm=toks[i].match(/^MP=(\d*)(?:\(\+(\d+))?$/);
				if(mpm){
					monde.MP=parseInt(mpm[1]);
					monde.PlusMP=parseInt(mpm[2]);
					i++;
				}
				state=8;
				break;
			case 8:
				let exm=toks[i].match(/^E=(\d)\/(\d+)+$/);
				if(exm){
					monde.NbExplo=parseInt(exm[1]);
					i++;
				}
				state=9;
				break;
			case 9:
				let pem=toks[i].match(/^PE=\d+$/);
				if(pem){
					monde.PotExplo=parseInt(pem[1]);
					i++;
				}
				state=10;
				break;
			case 10: //flotte 
				if(toks[i].charAt(0)=='F'){
					let flotte=new Flotte(toks[i],tour,{});
					flottes.push(flotte);
					i++;
					state=11;
				}else 
					state=20;
				break;
			case 11: //nom
				if(toks[i].charAt(0)=='"'){

				}else if(toks[i].charAt(0)=='('){

				}
				state=12;
				break;
			case 20:// trace
				i++;
				break;
			case 100:
				return null;
			}
		}
		console.log(monde);
		return {t:numt,m:monde,f:flottes};
	}
	
	static parseTour(strnbt:string){
		let lines=strnbt.split("\n");
		let state=0;
		let tour:any={};
		let id=0;
		for(let ll of lines){
			let l=ll.trim();
			if(l.startsWith('[')){
				console.log("\""+l+"\"");
				switch(l){
				case "[GENERAL]":
					state=1;
					console.log("1");
					if(!tour.gen)
						tour.gen={};
					break;
				case "[TECHNO]":
					state=2;
					console.log("2");
					if(!tour.tech)
						tour.tech={};
					break;
				default:
					switch(l.substring(1,3)){
					case "J_":
						state=3;
						console.log(l);
						id=parseInt(l.substring(3,l.length-1));
						if(!tour.j)
							tour.j={};
						if(!tour.j['J_'+id])
							tour.j['J_'+id]={};
						break;
					case "M_":
						state=4;
						console.log(l);
						id=parseInt(l.substring(3,l.length-1));
						if(!tour.m)
							tour.m={};
						if(!tour.m['M_'+id])
							tour.m['M_'+id]={};
						break;
					case "F_":
						state=5;
						console.log(l);
						id=parseInt(l.substring(3,l.length-1));
						if(!tour.f)
							tour.f={};
						if(!tour.f['F_'+id])
							tour.f['F_'+id]={};
						break;
					case "T_":
						state=6;
						console.log(l);
						id=parseInt(l.substring(3,l.length-1));
						if(!tour.t)
							tour.t={};
						if(!tour.t['T_'+id])
							tour.t['T_'+id]={};
						break;
					}
				}
			}else{
				let p=l.split("=",2);
				if(p.length==2)
					switch (state){
					case 1: // GENERAL
						tour.gen[p[0]]=p[1];
						break;
					case 2: // TECHNO
						tour.tech[p[0]]=p[1];
						break;
					case 3: // JOUEUR
						tour.j['J_'+id][p[0]]=p[1];
						break;
					case 4: // MONDE
						tour.m['M_'+id][p[0]]=p[1];
						break;
					case 5: // FLOTTE
						tour.f['F_'+id][p[0]]=p[1];
						break;
					case 6: // TRESOR
						tour.t['T_'+id][p[0]]=p[1];
						break;
					}
			}
		}
		return tour;
	}

	static buildNBT(tour: any){

	}
}