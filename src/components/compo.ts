
export interface ICompo{
	elem:HTMLElement;
}
export class Compo implements ICompo{
	main:ICompo;
	elem:HTMLElement;
	tagName:string='div';
	constructor(main:ICompo){
		this.main=main;
		this.elem=document.createElement(this.tagName);
		this.main.elem.appendChild(this.elem);
	}
	remove(){
		if(this.elem){
			if(this.main.elem.contains(this.elem))
				this.main.elem.removeChild(this.elem);
			delete this.elem;
		}
	}
	addTag(parent:HTMLElement,name:string):HTMLElement{
		let tag=document.createElement(name);
		parent.appendChild(tag);
		return tag;
	}
}