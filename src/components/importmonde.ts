import { ICompo,Compo } from './compo';
import { Popup } from './popup';

export class ImportMonde extends Popup {
	form:HTMLElement;
	input:HTMLInputElement;
	submit:HTMLElement;
	click:any;
	constructor(main:ICompo){
		super(main);
		this.title.innerHTML='Paste CR';
		this.form=this.addTag(this.elem,'form');
		this.input=<HTMLInputElement>this.addTag(this.form,'textarea');
		this.submit=this.addTag(this.form,'input');
		this.submit.setAttribute('type','submit');
		let _this=this;
		this.click=function(e:Event){
			e.preventDefault();
			this.parse();
			return false;
		}.bind(this);
		this.form.addEventListener("submit",this.click);
	}
	remove(){
		this.form.removeEventListener("submit",this.click);
		super.remove();
	}
	parse(){
		document.dispatchEvent(new CustomEvent("addMondeCR",{detail:this.input.value}));			
	}
}