import { GMap } from './gmap';
import { Node } from './node';

export class NodeMenu{
	map:GMap;
	g:SVGElement;
	p:SVGElement;
	f:SVGElement;
	ebtn:SVGElement;
	dbtn:SVGElement;
	div:HTMLElement;
	cnt:HTMLElement;
	cmt:HTMLElement;
	cmtedt:HTMLTextAreaElement;
	cmtedtbtn:HTMLElement;
	eventListener:any;
	idnode:string;
	static addDefs(map:GMap){
		map.addDef('path','pth_menu').setAttribute('d','M 0 -12 L 12 -30 L 60 -30 L 65 -25 L 65 -10 L 20 -10 L 20 20 L 12 20 L 0 12 A 12 12 0 0 0 0 -12');//'M 0 -12 L 12 -32 L 32 -32 L 32 32 L 12 32 L 0 12 A 12 12 0 0 0 0 -12');
	}
	constructor(map:GMap){
		this.map=map;
		this.eventListener=function(e:CustomEvent){
			this.openMenu(e.detail);
		}.bind(this);
		document.addEventListener("openMenu",this.eventListener);
		this.g=this.map.menulayer.addTag('g');
		this.g.setAttribute('visibility','hidden');
		this.g.onmousedown=function(e:Event){
			e.stopPropagation();
		};
		this.p=this.map.addTagTo(this.g,'use');
		this.p.setAttribute('href','#pth_menu');
		this.p.classList.add('menupath');
		this.ebtn=this.map.addTagTo(this.g,'use');
		this.ebtn.setAttribute('href','#edit');
		this.ebtn.setAttribute('transform','translate(20 -28) scale(0.7)');
		this.ebtn.onclick=function(e:Event){
			this.showEdit();
		}.bind(this);
		this.dbtn=this.map.addTagTo(this.g,'use');
		this.dbtn.setAttribute('href','#delete');
		this.dbtn.setAttribute('transform','translate(40 -28) scale(0.7)');
		this.dbtn.onclick=function(e:Event){
			e.stopPropagation();
			if(window.confirm("enlever le monde ?"))
				document.dispatchEvent(new CustomEvent("deleteNode",{detail:{id:this.idnode}}))
			this.close();
		}.bind(this);
		this.f=this.map.addTagTo(this.g,'foreignObject');
		this.f.setAttribute('x','13');
		this.f.setAttribute('y','-10');
		this.div=document.createElement('div');
		this.div.classList.add('nodemenudiv')
		this.f.appendChild(this.div);
/*		this.btn=this.map.addHTagTo(this.div,'button');
		this.btn.innerHTML="delete";
		this.btn.onclick=function(e:Event){
			e.stopPropagation();
			if(window.confirm("enlever le monde ?"))
				document.dispatchEvent(new CustomEvent("deleteNode",{detail:{id:this.idnode}}))
			this.close();
		}.bind(this);
*/		this.cnt=this.map.addHTagTo(this.div,'div');
		this.cmt=this.map.addHTagTo(this.div,'div');
		this.cmtedt=<HTMLTextAreaElement >this.map.addHTagTo(this.div,'textarea');
		this.cmtedtbtn=this.map.addHTagTo(this.div,'button');
		this.cmtedtbtn.innerHTML="Ok";
		this.cmtedtbtn.onclick=function(e:Event){
			e.stopPropagation();
			if(this.idnode){
				this.cmt.innerHTML=this.cmtedt.value;
				this.hideEdit();
				document.dispatchEvent(new CustomEvent("updateComment",{detail:{id:this.idnode,comment:this.cmtedt.value}}));
			}
		}.bind(this);
	}
	openMenu(node:Node){
		if(node.id=='M_116')
			console.log(node);
		this.idnode=node.id;
		this.g.setAttribute('transform','translate('+node.x+","+node.y+")");
		this.g.setAttribute('visibility','visible');
		this.f.setAttribute('width','50px');
		this.f.setAttribute('height','20px');
		this.cnt.innerHTML="";
		let pre=this.map.addHTagTo(this.cnt,'pre');
		if(node.tour){
			pre.innerHTML=node.tour.getCR()+'\n';
			this.dbtn.style.visibility='hidden';
		}
		else{
			this.dbtn.style.visibility='visible';
			pre.innerHTML="*** inconnu ***\n";	
		} 
		pre.innerHTML+=node.getFlottesCR();
		this.cmt.innerHTML=node.comment?node.comment:"";
		this.cmtedt.value=node.comment?node.comment:"";
		this.hideEdit();
	}
	showEdit(){
		this.cmtedt.style.display='none';
		this.cmtedt.style.display='block';
		this.cmtedtbtn.style.display='block';
		this.resize();
	}
	hideEdit(){
		this.cmtedt.style.display='block';
		this.cmtedt.style.display='none';
		this.cmtedtbtn.style.display='none';
		this.resize();
	}
	resize(){
		this.f.setAttribute('width',''+Math.max(100,this.div.scrollWidth+4)+'px');
		this.f.setAttribute('height',''+Math.max(this.div.scrollHeight+4)+'px');
		let rect:DOMRect=this.g.getBoundingClientRect();
		document.dispatchEvent(new CustomEvent('popupResize',{detail:{rect:rect}}));
	}
	close(){
		this.g.setAttribute('visibility','hidden');
		this.dbtn.style.visibility='hidden';
	}
}