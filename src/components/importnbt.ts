import { ICompo,Compo } from './compo';
import { Popup } from './popup';
import { NBT } from './nbt';

export class ImportNbt extends Popup {
	form:HTMLElement;
	input:HTMLInputElement;
	submit:HTMLElement;
	click:any;
	constructor(main:ICompo){
		super(main);
		this.title.innerHTML='Import NBT';
		this.form=this.addTag(this.elem,'form');
		this.input=<HTMLInputElement>this.addTag(this.form,'input');
		this.input.setAttribute('type','file');
		this.addTag(this.form,'br');
		this.submit=this.addTag(this.form,'input');
		this.submit.setAttribute('type','submit');
		let _this=this;
		this.click=function(e:Event){
			e.preventDefault();
			this.parse();
			return false;
		}.bind(this);
		this.form.addEventListener("submit",this.click);
	}
	remove(){
		this.form.removeEventListener("submit",this.click);
		super.remove();
	}
	parse(){
		let f:File = this.input.files[0];
		let  reader = new FileReader();
		console.log(f.name);
		reader.onload=function(e:any){
			let tour:any=NBT.parseTour(e.target.result);
			document.dispatchEvent(new CustomEvent("addTour",{detail:tour}));			
		}
		reader.readAsText(f,'ISO-8859-1');
	}
}