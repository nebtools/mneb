import { GMap } from './gmap';

export class Layer{
	map:GMap;
	g:SVGElement ;
	constructor(map:GMap,name:string){
		this.map=map;
		this.g=<SVGElement>document.createElementNS('http://www.w3.org/2000/svg','g');
		this.g.setAttribute("ln",name);
		this.map.svg.appendChild(this.g);
	}
	addTag(name:string):SVGElement{
		return this.map.addTagTo(this.g,name);
	}
	
	removeTag(tag:SVGElement){
		if(this.g.contains(tag))
			this.g.removeChild(tag);
	}
}