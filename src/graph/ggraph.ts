import {IGNodes,IGNode,IGLinks,IGLink} from './gmodel';

export class GGraph{
	svg:SVGSVGElement;
	nodes:IGNodes={};
	links:IGLinks={};
	linkslayer:GLayer;
	linklabslayer:GLayer;
	nodeslayer:GLayer;
	nodelabslayer:GLayer;
	menulayer:GLayer;
	constructor(private parent:HTMLElement){
		this.svg=<SVGSVGElement> document.createElementNS('svg','svg');
	}
	resize(){
		let rct=this.parent.getBoundingClientRect();
		this.svg.setAttribute("width",""+(rct.width-1));
		this.svg.setAttribute("height",""+(rct.height-1));
	}
	addGraph(nodes:IGNodes,links:IGLinks){
		for(let kn in nodes){
			if(this.nodes[kn])
				this.nodes[kn].merge(nodes[kn]);
			else
				this.nodes[kn]=nodes[kn];
		}
		for(let kl in links){
			if(this.links[kl])
				this.links[kl].merge(links[kl]);
			else
				this.links[kl]=links[kl];
		}
	}
	addTagTo(parent:SVGElement,name:string):SVGElement{
		let tag=<SVGElement>document.createElementNS('http://www.w3.org/2000/svg',name);
		parent.appendChild(tag);
		return tag;
	}
}
export class GLayer{
	g:SVGElement ;
	constructor(private main:GGraph){
		this.g=<SVGElement>document.createElementNS('http://www.w3.org/2000/svg','g');
		this.main.svg.appendChild(this.g);
	}

	addTag(name:string):SVGElement{
		return this.main.addTagTo(this.g,name);
	}
		
	removeTag(tag:SVGElement){
		if(this.g.contains(tag))
			this.g.removeChild(tag);
	}
}
