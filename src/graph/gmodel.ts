export interface IGLink {
	merge(link:IGLink):void;
}
export interface IGLinks {
	[index:string]:IGLink;
}

export interface IGNode{
	merge(node:IGNode):void;
}
export interface IGNodes {
	[index:string]:IGNode;
}
