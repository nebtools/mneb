export class Techno{
	props:any={};
	constructor(jtech:any){
		for(let kg in jtech){
			this.props[kg]=jtech[kg];
		}
	}
	toNBT():string{
		let str="";
		for(let kg in this.props){
			str+=kg+"="+this.props[kg]+"\n";
		}
		return str;
	}
}