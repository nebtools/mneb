export class Tresor{
	props:any={};
	constructor(jt:any){
		for(let kg in jt){
			this.props[kg]=jt[kg];
		}
	}
	toNBT():string{
		let str="";
		for(let kg in this.props){
			str+=kg+"="+this.props[kg]+"\n";
		}
		return str;
	}
}
export interface ITresors{
	[index:string]:Tresor;
}