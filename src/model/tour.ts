import {Joueur,IJoueurs} from './joueur';
import {Monde,IMondes} from './monde';
import {Flotte,IFlottes} from './flotte';
import {Tresor,ITresors} from './tresor';
import {Techno} from './techno';
import {General} from './general';
export class Tour {
	g:General;
	tech:Techno;
	j:IJoueurs={};
	m:IMondes={};
	f:IFlottes={};
	t:ITresors={};
	constructor(jt:any){
		this.g=new General(jt.gen);
		this.tech=new Techno(jt.tech);
		for(let kj in jt.j){
			this.j[kj]=new Joueur(kj,jt.j[kj]);
		}
		for(let kn in jt.m){
			this.m[kn]=new Monde(kn,this,jt.m[kn]);
		}
		for(let kf in jt.f){
			this.f[kf]=new Flotte(kf,this,jt.f[kf]);
		}
		for(let kt in jt.t){
			this.t[kt]=new Tresor(jt.t[kt]);
		}

	}
	toJson(){
		let resp:any={};
		resp.gen=this.g.toJson();
		resp.j={};
		resp.m={};
		resp.f={};
		for(let kj in this.j){
			resp['j'][kj]=this.j[kj].toJson();
		}
		for(let km in this.m){
			resp['m'][km]=this.m[km].toJson();
		}
		for(let kf in this.f){
			resp['f'][kf]=this.f[kf].toJson();
		}
		return resp;
	}
	toNBT():string{
		let str="";
		str+="[GENERAL]\n"+this.g.toNBT()+"\n";
		str+="[TECHNO]\n"+this.tech.toNBT()+"\n";
		for(let kj in this.j)
			str+="["+kj+"]\n"+this.j[kj].toNBT();
		for(let km in this.m)
			str+="["+km+"]\n"+this.m[km].toNBT();
		for(let kf in this.f)
			str+="["+kf+"]\n"+this.f[kf].toNBT();
		for(let kt in this.t)
			str+="["+kt+"]\n"+this.t[kt].toNBT();
		return str;
	}
	getIdJoueur(nom:string):string{
		console.log("get "+nom);
		for(let kj in this.j){
			console.log(this.j[kj]);
			if(this.j[kj].Pseudo==nom)
				return kj;
		}
		return "";
	}
}