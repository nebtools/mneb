import {INode} from './inode';

export interface ILink {
	from:INode;
	to:INode;
	remove():void;
	show():void;
}

export interface ILinks {
	[index:string]:ILink;
}