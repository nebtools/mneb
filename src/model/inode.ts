import {Monde} from './monde';
import {Flotte} from './flotte';
import {ILinks} from './ilink';

export interface INode {
	id:string;
	x:number;
	y:number;
	readonly label : string;
	cn:number[];
	links : ILinks;
	remove():void;
	show():void;
	changeZoom():void;
	setTour(m:Monde):void;
	addFlotte(f:Flotte):void;
	addTrace(f:Flotte):void;
}

export interface INodes {
	[index:string]:INode;
}