import {NebDt} from './nebdt';
import {Tour} from './tour';

export class Monde extends NebDt{
	id:string;
	x:number;
	y:number;
	cn:number[];
	Nom:string;
	Duree:number;
	ProprConv:number;
	PlusMP:number;
	PC:number;
	Proprio:number;
	Ind:number;
	Pop:number;
	Conv:number;
	Robot:number;
	MaxPop:number;
	MP:number;
	VI:number;
	VP:number;
	CibleVI:number;
	CibleVP:number;
	ActionVI:number;
	ActionVP:number;
	Pi:number;
	RecupPi:number;
	AncienProprio:number;
	IndLibre:number;
	PopLibre:number;
	Capture:boolean;
	Cadeau:boolean;
	TN:boolean;
	Bombe:boolean;
	Pille:boolean;
	PremierProprioJihad:boolean;
	NbExplo:number;
	NbExploCM:number;
	NbExploPop:number;
	NbExploLoc:number;
	Localise:number;
	PotExplo:number;
	PopMoins:number;
	ConvMoins:number;
	RobMoins:number;
	EPC:number;
	hasM:boolean;
	hasCoord:boolean;
	hasC:boolean;
	hasEx:boolean;
	hasME:boolean;
	tour:Tour;
	constructor(id:string,tour:Tour,jm:any){
		super();
		this.id=id;
		this.tour=tour;
		if(jm.Coord){
			let cc=jm.Coord.split(",");
			this.hasCoord=true;
			this.x=this.parseInt(cc[0]);
			this.y=this.parseInt(cc[1]);
		}
		this.cn=[];
		if(jm.C){
			this.hasC=true;
			let cn=jm.C.split(",");
			for(let i=0;i<cn.length;i++)
				this.cn.push(this.parseInt(cn[i]));
		}
		this.Nom=jm.Nom;
		if(jm.M){
			let mm=jm.M.split(",");
			this.parseM(mm);
		}
		if(jm.Explo){
			let mex=jm.Explo.split(',');
			this.parseEx(mex);
		}
		if(jm.ME){
			let me=jm.ME.split(',');
			this.parseME(me);
		}
	}
	parseM(mm:string[]){
		this.hasM=true;
		this.Duree = this.parseInt(mm[0]);
		this.ProprConv = this.parseInt(mm[1]);
		this.PlusMP = this.parseInt(mm[2]);
		this.PC = this.parseInt(mm[3]);
		this.Proprio = this.parseInt(mm[4]);
		this.Ind = this.parseInt(mm[5]);
		this.Pop = this.parseInt(mm[6]);
		this.Conv = this.parseInt(mm[7]);
		this.Robot = this.parseInt(mm[8]);
		this.MaxPop = this.parseInt(mm[9]);
		this.MP = this.parseInt(mm[10]);
		this.VI = this.parseInt(mm[11]);
		this.VP = this.parseInt(mm[12]);
		this.Pi = this.parseInt(mm[13]);
		this.RecupPi = this.parseInt(mm[14]);
		this.AncienProprio = this.parseInt(mm[15]);
		this.IndLibre = this.parseInt(mm[16]);
		this.PopLibre = this.parseInt(mm[17]);
		this.Capture = this.parseBool(mm[18]);
		this.Cadeau = this.parseBool(mm[19]);
		this.TN = this.parseBool(mm[20]);
		this.Bombe = this.parseBool(mm[21]);
		this.Pille = this.parseBool(mm[22]);
		this.PremierProprioJihad = this.parseBool(mm[23]);
	}
	parseEx(mex:string[]){
		this.hasEx=true;
		this.NbExplo = this.parseInt(mex[0]);
        this.NbExploCM = this.parseInt(mex[1]);
        this.NbExploPop = this.parseInt(mex[2]);
        this.NbExploLoc = this.parseInt(mex[3]);
        this.Localise = this.parseInt(mex[4]);
        //AVuConnect[monde, NoJou] := Boolean(StrToInt(DataList[4]));
        //StationMonde[monde, NoJou] := Boolean(StrToInt(DataList[5]));
        //if NoJou > 0 then
        //  Connu[monde, NoJou] := Boolean(StrToInt(DataList[6]));
        this.PotExplo = this.parseInt(mex[7]);
	}
	parseME(me:number[]){
		this.hasME=true;
		this.ActionVI=me[0];
		this.ActionVP=me[1];
		this.CibleVI=me[2];
		this.CibleVP=me[3];
		this.PopMoins=me[4];
		this.ConvMoins=me[5];
		this.RobMoins=me[7];
		this.EPC=me[8];
	}
	toJson(){
		let jm:any={};
		if(this.hasCoord){
			jm["Coord"]=""+this.x+','+this.y;
		}
		if(this.hasC){
			jm["C"]=this.cn.join(',');
		}
		if(this.hasM){
			let str="";
			str+=this.addOrNot(this.Duree)+',';
			str+=this.addOrNot(this.ProprConv)+',';
			str+=this.addOrNot(this.PlusMP)+',';
			str+=this.addOrNot(this.PC)+',';
			str+=this.addOrNot(this.Proprio)+',';
			str+=this.addOrNot(this.Ind)+',';
			str+=this.addOrNot(this.Pop)+',';
			str+=this.addOrNot(this.Conv)+',';
			str+=this.addOrNot(this.Robot)+',';
			str+=this.addOrNot(this.MaxPop)+',';
			str+=this.addOrNot(this.MP)+',';
			str+=this.addOrNot(this.VI)+',';
			str+=this.addOrNot(this.VP)+',';
			str+=this.addOrNot(this.Pi)+',';
			str+=this.addOrNot(this.RecupPi)+',';
			str+=this.addOrNot(this.AncienProprio)+',';
			str+=this.addOrNot(this.IndLibre)+',';
			str+=this.addOrNot(this.PopLibre)+',';
			str+=this.addOrNot(this.Capture?1:0)+',';
			str+=this.addOrNot(this.Cadeau?1:0)+',';
			str+=this.addOrNot(this.TN?1:0)+',';
			str+=this.addOrNot(this.Bombe?1:0)+',';
			str+=this.addOrNot(this.Pille?1:0)+',';
			str+=this.addOrNot(this.PremierProprioJihad?1:0);
			jm["M"]=str;
		}
		if(this.hasME){
			let str='';
			str+=this.addOrNot(this.ActionVI)+',';
			str+=this.addOrNot(this.ActionVP)+',';
			str+=this.addOrNot(this.CibleVI)+',';
			str+=this.addOrNot(this.CibleVP)+',';
			str+=this.addOrNot(this.PopMoins)+',';
			str+=this.addOrNot(this.ConvMoins)+',';
			str+=this.addOrNot(this.RobMoins)+',';
			str+=this.addOrNot(this.EPC);
			jm['ME']=str;
		}
		if(this.hasEx){
			let str="";
			str+=this.addOrNot(this.NbExplo)+',';
			str+=this.addOrNot(this.NbExploCM)+',';
			str+=this.addOrNot(this.NbExploPop)+',';
			str+=this.addOrNot(this.NbExploLoc)+',';
			str+=this.addOrNot(this.Localise)+',';
			str+=this.addOrNot(this.PotExplo)+'\n';
			jm["Explo"]=str;
		}
		return jm;
	}
	getCR():string{
		let str="";
		if(this.Nom)
			str+=this.Nom+'\n';
		if(this.Bombe)
			str+="M#"+this.id.substr(2);
		else
			str+=this.id+' ';
		if(this.TN)
			str+="*** Trou noir ***";
		else{
			str+='('+this.cn[0];
			for(let i=1;i<this.cn.length;i++)
				str+=','+this.cn[i];
			str+=') ';
			if(this.MaxPop){
				if(this.Proprio || this.AncienProprio){
					str+='"';
					if(this.Proprio)
						str+=this.tour.j['J_'+this.Proprio].Pseudo;
					if(this.AncienProprio && this.AncienProprio!= this.Proprio)
						str+='('+this.tour.j['J_'+this.AncienProprio].Pseudo+')';
					if(this.Proprio && this.Duree>0)
						str+=':'+this.Duree;
					str+='"';
				}
				if(this.Capture)
					str+='!';
				if(this.Pille)
					str+='P';
				if(this.Cadeau)
					str+='C';
				str+=' '
				if(this.VI)
					str+='[';
				if(this.Ind>0)
					str+='I=';
				if(this.IndLibre<this.Ind)
					str+=this.IndLibre+'/';
				if(this.Ind)
					str+=this.Ind;
				if(this.VI)
					str+=']='+this.VI;
				if(this.Ind||this.VI)
					str+=' ';
				if(this.VP)
					str+='[';
				str+='P=';
				if(!this.Robot)
					str+=this.Pop;
				if(this.Pop && this.Conv==this.Pop)
					str+='C';
				if(this.Robot)
					str+=this.Robot+'R';
				str+='('+this.MaxPop+')';
				if(this.Conv&&(this.Conv!=this.Pop||this.Proprio!=this.ProprConv)){
					str+='/'+this.Conv+'C';
					if(this.Proprio!=this.ProprConv)
						str+=':'+this.tour.j['J_'+this.ProprConv].Pseudo;
				}
				if(this.VP)
					str+=']='+this.VP;
				if(this.Pop|| this.Robot)
					str+=' ';
				if(this.MP ||this.PlusMP)
					str+='MP=';
				if(this.MP)
					str+=this.MP;
				if(this.PlusMP)
					str+='('+this.PlusMP+')';

				if(this.Pi){
					str+='Pi='+this.Pi+'/'+this.RecupPi;
				}
			} else
				str+="*** INCONNU ***";
		}
		return str;
	}
	addOrNot(n:number){
		if(n>0)
			return ""+n;
		return "";
	}
	toNBT():string{
		let str="";
		if(this.hasCoord){
			str+="Coord="+this.x+','+this.y+'\n';
		}
		if(this.hasC){
			str+="C="+this.cn[0];
			for(let i=1;i<this.cn.length;i++)
				str+=','+this.cn[i];
			str+='\n';
		}
		if(this.hasM){
			str+="M=";
			str+=this.addOrNot(this.Duree)+',';
			str+=this.addOrNot(this.ProprConv)+',';
			str+=this.addOrNot(this.PlusMP)+',';
			str+=this.addOrNot(this.PC)+',';
			str+=this.addOrNot(this.Proprio)+',';
			str+=this.addOrNot(this.Ind)+',';
			str+=this.addOrNot(this.Pop)+',';
			str+=this.addOrNot(this.Conv)+',';
			str+=this.addOrNot(this.Robot)+',';
			str+=this.addOrNot(this.MaxPop)+',';
			str+=this.addOrNot(this.MP)+',';
			str+=this.addOrNot(this.VI)+',';
			str+=this.addOrNot(this.VP)+',';
			str+=this.addOrNot(this.Pi)+',';
			str+=this.addOrNot(this.RecupPi)+',';
			str+=this.addOrNot(this.AncienProprio)+',';
			str+=this.addOrNot(this.IndLibre)+',';
			str+=this.addOrNot(this.PopLibre)+',';
			str+=this.addOrNot(this.Capture?1:0)+',';
			str+=this.addOrNot(this.Cadeau?1:0)+',';
			str+=this.addOrNot(this.TN?1:0)+',';
			str+=this.addOrNot(this.Bombe?1:0)+',';
			str+=this.addOrNot(this.Pille?1:0)+',';
			str+=this.addOrNot(this.PremierProprioJihad?1:0)+'\n';
		}
		if(this.hasME){
			str+="ME=";
			str+=this.addOrNot(this.ActionVI)+',';
			str+=this.addOrNot(this.ActionVP)+',';
			str+=this.addOrNot(this.CibleVI)+',';
			str+=this.addOrNot(this.CibleVP)+',';
			str+=this.addOrNot(this.PopMoins)+',';
			str+=this.addOrNot(this.ConvMoins)+',';
			str+=this.addOrNot(this.RobMoins)+',';
			str+=this.addOrNot(this.EPC)+'\n';
		}
		if(this.hasEx){
			str+="Explo=";
			str+=this.addOrNot(this.NbExplo)+',';
			str+=this.addOrNot(this.NbExploCM)+',';
			str+=this.addOrNot(this.NbExploPop)+',';
			str+=this.addOrNot(this.NbExploLoc)+',';
			str+=this.addOrNot(this.Localise)+',';
			str+=this.addOrNot(this.PotExplo)+'\n';
		}
		return str;
	}
}

export interface IMondes{
	[index:string]:Monde;
}